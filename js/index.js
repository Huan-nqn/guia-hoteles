$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 5000
    });
    $('#contacto').on('show.bs.modal', function (e) {
      console.log("El modal contacto se está mostrando");
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-success');
      $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
      console.log("El modal contacto se mostró");
    });
    $('#contacto').on('hide.bs.modal', function (e) {
      console.log("El modal contacto se está ocultando");
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
      console.log("El modal contacto se ocultó");
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').removeClass('btn-success');
      $('#contactoBtn').prop('disabled', false);
    });
  });